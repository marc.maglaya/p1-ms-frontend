def buildImage(String buildID) {
    sh "docker build -t registry.gitlab.com/marc.maglaya/p1-ms-frontend:${buildID} ."
}

def pushImage() {
    withCredentials([usernamePassword(credentialsId: 'gitlab-credentials', passwordVariable: 'PASS', usernameVariable: 'USER')]) {        
        sh "echo ${PASS} | docker login -u ${USER} --password-stdin registry.gitlab.com"
        sh "docker push registry.gitlab.com/marc.maglaya/p1-ms-frontend:${BUILD_ID}"
    }
}


return this